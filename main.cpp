#include <QApplication>
#include <QHBoxLayout>
#include <QWidget>
#include <QPlainTextEdit>
#include <QWebEngineView>
#include <QString>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QWidget *window = new QWidget;
    window->resize(600,300);
    QHBoxLayout *layout = new QHBoxLayout(window);

    QPlainTextEdit* textEdit = new QPlainTextEdit;
    QWebEngineView *webView = new QWebEngineView;
    webView->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    layout->addWidget(textEdit);
    layout->addWidget(webView);
    QObject::connect(textEdit, &QPlainTextEdit::textChanged, [textEdit,webView](){
        webView->setHtml(textEdit->document()->toPlainText());
    });

    window->show();
    return a.exec();
}
